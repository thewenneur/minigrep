use std::{env, error::Error, fs};

fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents.lines().filter(|line| line.to_lowercase().contains(
        query.to_lowercase().as_str()
    )).collect()
}

fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    contents.lines().filter(|line| line.contains(query)).collect()
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    let contents = fs::read_to_string(config.filename)?;

    let results = if config.ignore_case {
        search_case_insensitive(&config.query, &contents)
    } else {
        search(&config.query, &contents)
    };
    for line in results {
        println!("{}", line);
    }
    Ok(())
}

/// # Query configuration object.
/// This is the object that must be instanced before using any method \
/// within this crate.
/// ## Example
/// ```
///  use std::{env, process};
///  use minigrep::Config;
///
///  let iter = vec!["Useless str", "to", "./poem.txt"];
///  let iter = iter.iter().map(|x| x.to_string());
///  let config = Config::new(iter).unwrap_or_else(|err| {
///         eprintln!("Problem parsing arguments: {}", err);
///         process::exit(1);
///  });
///
/// ```
pub struct Config {
    query: String,
    filename: String,
    ignore_case: bool
}

impl Config {
    ///Creating a new instance of minigrep::Config
    pub fn new( mut args: impl Iterator<Item = String>) -> Result<Config, &'static str> {

        args.next();
        let query = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a query string"),
        };

        let filename = match args.next() {
            Some(arg) => arg,
            None => return Err("Didn't get a file name"),
        };

        let ignore_case = env::var("IGNORE_CASE").is_ok();

        Ok(Config {
            query,
            filename,
            ignore_case,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn case_insensitive() {
        let query = "rUsT";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";

        assert_eq!(
            vec!["Rust:", "Trust me."],
            search_case_insensitive(query, contents)
        );
    }
}
